<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class SummaryOfOrganization extends DB{
    public $id="";
    public $summary_of_organization="";
    public $author_name="";

    public function __construct(){
        parent:: __construct();
        if(!isset($_SESSION)) session_start();
    }

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('summary_of_organization',$postVariableData)){
            $this->summary_of_organization = $postVariableData['summary_of_organization'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
    }



    public function store(){

        $arrData = array( $this->summary_of_organization, $this->author_name);

        $sql = "Insert INTO summary_of_organization(summary_of_organization, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<h3>Success! Data Has Been Inserted Successfully :)</h3>");
        else
            Message::message("<h3>Failed! Data Has Not Been Inserted Successfully :( </h3>");

        Utility::redirect('create.php');


    }// end of store method







    public function index()
    {

    }




}

?>

